// timerfd.h
//  lightweight wrappers around the system calls for platforms that support
//  them, but use GLIBC < 2.8

#ifndef _SYS_TIMERFD_H
#define _SYS_TIMERFD_H 1

// define syscall numbers
//   see arch/arm/include/asm/unistd.h
//   https://elixir.bootlin.com/linux/v2.6.39.4/source/arch/arm/include/asm/unistd.h#L379
#ifndef __NR_SYSCALL_BASE
#define __NR_SYSCALL_BASE 0
#endif
#ifndef __NR_timerfd_create
#define __NR_timerfd_create __NR_SYSCALL_BASE + 350
#endif
#ifndef __NR_timerfd_settime
#define __NR_timerfd_settime __NR_SYSCALL_BASE + 353
#endif
#ifndef __NR_timerfd_gettime
#define __NR_timerfd_gettime __NR_SYSCALL_BASE + 354
#endif


#include <unistd.h>
#include <syscall.h>

#include <time.h>

#include <fcntl.h>
#define TFD_TIMER_ABSTIME (1 << 0)
#define TFD_TIMER_CANCEL_ON_SET (1 << 1)
#define TFD_CLOEXEC O_CLOEXEC
#define TFD_NONBLOCK O_NONBLOCK
#define TFD_SHARED_FCNTL_FLAGS (TFD_CLOEXEC | TFD_NONBLOCK)
#define TFD_CREATE_FLAGS TFD_SHARED_FCNTL_FLAGS
#define TFD_SETTIME_FLAGS (TFD_TIMER_ABSTIME | TFD_TIMER_CANCEL_ON_SET)


int timerfd_create( int clockid, int flags );
int timerfd_settime( int fd, int flags,
        const struct itimerspec * new_value,
        struct itimerspec * old_value );
int timerfd_gettime( int fd, struct itimerspec * curr_value );


int timerfd_create( int clockid, int flags ) {
    return syscall( __NR_timerfd_create, clockid, flags );
}


int timerfd_settime( int fd, int flags,
        const struct itimerspec * new_value,
        struct itimerspec * old_value ) {
    return syscall( __NR_timerfd_settime, fd, flags, new_value, old_value );
}


int timerfd_gettime( int fd, struct itimerspec * curr_value ) {
    return syscall( __NR_timerfd_gettime, fd, curr_value );
}


#endif // _SYS_TIMERFD_H
