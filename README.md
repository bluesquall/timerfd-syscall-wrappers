timerfd-syscall-wrappers
========================

A small package to add lightweight wrappers around the timerfd system calls
for platforms that support them, but use GLIBC < 2.8 -- only useful for old
systems.
